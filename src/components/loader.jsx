import React, { Component } from "react";

class Loader extends Component {
  state = {};
  render() {
    return (
      <div id="load" class="wrap">
        <div class="lds-ring">
          <div></div>
          <div></div>
          <div></div>
          <div></div>
        </div>
      </div>
    );
  }
}

export default Loader;
