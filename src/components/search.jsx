import React, { Component } from "react";

class Search extends Component {
  state = {};
  render() {
    return (
      <div className="search">
        <form>
          <input type="search" onChange={(query) => this.handleSearch(query)} />
        </form>
      </div>
    );
  }
}

export default Search;
