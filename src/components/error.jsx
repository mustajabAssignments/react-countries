import React, { Component } from "react";

class Error extends Component {
  state = {};
  render() {
    return (
      <>
        <div className="container">
          <img src="error.png" alt="earth" />
          <h3>OOPS! An Error Occured.</h3>
        </div>
      </>
    );
  }
}

export default Error;
