import React, { Component } from "react";

import Countries from "./countries";

class App extends Component {
  state = {};
  render() {
    return (
      <>
        <header>
          <h1>Where In The World ?</h1>
        </header>
        <Countries />
      </>
    );
  }
}

export default App;
