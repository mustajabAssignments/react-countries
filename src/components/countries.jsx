import React, { Component } from "react";
import Loader from "./loader";
import Error from "./error";

const countriesAPI = "https://restcountries.com/v3.1/all";

class Countries extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loader: true,
      all: [],
      data: [],
    };
  }

  fetchData(url) {
    return fetch(url).then((response) => {
      if (response.ok) {
        return response.json();
      } else {
        return new Error("404 Not Found");
      }
    });
  }

  componentDidMount() {
    this.fetchData(countriesAPI)
      .then((countriesArr) => {
        this.setState({
          loader: false,
          all: [...countriesArr],
          data: [...countriesArr],
        });
      })
      .catch((error) => console.log(error));
  }

  renderCountries() {
    let list = [...this.state.data];
    return list;
  }

  handleChange = (event) => {
    const filter = event.target.value;

    if (filter === "all") {
      this.setState({
        data: this.state.all,
      });
    } else {
      const filteredData = this.state.all.filter((country) => {
        return country.continents[0].toLowerCase().includes(filter);
      });
      this.setState({
        data: filteredData,
      });
    }
  };

  handleSearch = (query) => {
    const filter = query.target.value.toLowerCase();

    const filteredData = this.state.data.filter((country) => {
      const capital = country.capital
        ? Object.values(country.capital).join(", ").toLowerCase()
        : "";
      const name = country.name.common.toLowerCase();
      return name.includes(filter) || capital.includes(filter);
    });
    this.setState({
      data: filteredData,
    });
  };

  render() {
    return (
      <>
        <div className="filters">
          <div className="region">
            <select onChange={this.handleChange}>
              <option value="all">Filter By Region</option>
              <option value="africa">Africa</option>
              <option value="america">America</option>
              <option value="asia">Asia</option>
              <option value="europe">Europe</option>
              <option value="oceania">Oceania</option>
            </select>
          </div>

          <div className="search">
            <form>
              <input
                type="search"
                onChange={(query) => this.handleSearch(query)}
                placeholder="Search..."
              />
            </form>
          </div>
        </div>

        {this.state.loader && <Loader />}
        <div className="content">
          {this.state.data.map((country) => {
            return (
              <div key={country.cca3} className="container">
                <img src={country.flags.png} alt="Flag" />
                <h3>{country.name.common}</h3>
                <ul>
                  <li>Population: {country.population}</li>
                  <li>Region: {country.continents}</li>
                  <li>Capital: {country.capital}</li>
                </ul>
              </div>
            );
          })}
        </div>
      </>
    );
  }
}

export default Countries;
