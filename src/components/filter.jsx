import React, { Component } from "react";

class Filter extends Component {
  state = {
    all: this.props.data,
    data: this.props.data,
  };
  handleChange = (event) => {
    const filter = event.target.value;

    if (filter === "all") {
      this.setState({
        data: this.state.all,
      });
    } else {
      const filteredData = this.state.all.filter((country) => {
        return country.continents[0].toLowerCase().includes(filter);
      });
      this.setState({
        data: filteredData,
      });
    }
  };
  render() {
    return (
      <div className="filter">
        <select onChange={this.handleChange}>
          <option value="all">Filter By Region</option>
          <option value="africa">Africa</option>
          <option value="america">America</option>
          <option value="asia">Asia</option>
          <option value="europe">Europe</option>
          <option value="oceania">Oceania</option>
        </select>
      </div>
    );
  }
}

export default Filter;
